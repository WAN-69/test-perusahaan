<?php

namespace App\Controllers;

use App\Models\BarangModel;

class Barang extends BaseController
{
    public function index()
    {
        $model = new BarangModel();
        $data['barang'] = $model->getProduct()->getResult();
        $data['kategori'] = $model->getCategory()->getResult();
        return view('index', $data);
    }

    public function simpan()
    {
        $model = new BarangModel();
        $data = array(
            'nama_product' => $this->request->getPost('nama_product'),
            'harga_product' => $this->request->getPost('harga_product'),
            'product_kategori_id' => $this->request->getPost('product_kategori')
        );
        $model->saveProduct($data);
        return redirect()->to('/barang');
    }

    public function edit()
    {
        $model = new BarangModel();
        $id = $this->request->getPost('product_id');
        $data = array(
            'nama_product' => $this->request->getPost('nama_product'),
            'harga_product' => $this->request->getPost('harga_product'),
            'product_kategori_id' => $this->request->getPost('product_kategori_id'),
        );
        $model->updateProduct($data, $id);
        return redirect()->to('/barang');
    }

    public function hapus()
    {
        $model = new BarangModel();
        $id = $this->request->getPost('product_id');
        $model->deletProduct($id);
        return redirect()->to('/barang');
    }
}
