<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Barang extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'product_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true
            ],
            'nama_product' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'harga_product' => [
                'type' => 'INT',
            ],
            'product_kategori_id' => [
                'type' => 'INT'
            ]
        ]);

        $this->forge->addPrimaryKey('product_id', TRUE);
        $this->forge->createTable('product', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('product');
    }
}
