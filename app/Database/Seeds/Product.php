<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Product extends Seeder
{
    public function run()
    {
        $products = [
            [
                'nama_product' => 'Handphone',
                'harga_product'  => 15000000,
                'product_kategori_id' => 1
            ],
            [
                'nama_product' => 'TV',
                'harga_product' => 2000000,
            ],
            [
                'nama_product' => 'Mouse',
                'harga_product'    => 30000,
            ]
        ];

        foreach ($products as $data) {
            // insert semua data ke tabel
            $this->db->table('product')->insert($data);
        }
    }
}
