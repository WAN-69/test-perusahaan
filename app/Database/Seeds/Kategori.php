<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Kategori extends Seeder
{
    public function run()
    {
        $kategoris = [
            [
                'nama_kategori' => 'Elektronik',
            ],
            [
                'nama_kategori' => 'Pakaian',
            ],
            [
                'nama_kategori' => 'Makanan',
            ]
        ];

        foreach ($kategoris as $data) {
            $this->db->table('kategori')->insert($data);
        }
    }
}
