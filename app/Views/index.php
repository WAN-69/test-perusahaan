<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aplikasi CRUD Sederhana</title>
    <link rel="stylesheet" href="/css/bootstrap.css">
</head>

<body>
    <div class="container">
        <div class="card mt-lg-5">
            <div class="card-header">
                <h3>Daftar Barang</h3>
                <button class="btn btn-success d-flex justify-content-center" data-toggle="modal" data-target="#addModal">+ Tambah Barang Baru</button>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Nama Barang</th>
                            <th>Harga</th>
                            <th>Kategori</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($barang as $item) : ?>
                            <tr>
                                <td><?= $item->nama_product; ?></td>
                                <td><?= $item->harga_product; ?></td>
                                <td><?= $item->nama_kategori; ?></td>
                                <td>
                                    <a href="#" class="btn btn-info btn-sm btn-edit" data-id="<?= $item->product_id; ?>" data-name="<?= $item->nama_product; ?>" data-price="<?= $item->harga_product; ?>" data-category_id="<?= $item->product_kategori_id; ?>">Edit Barang</a>
                                    <a href="#" class="btn btn-danger btn-sm btn-delete" data-id="<?= $item->product_id; ?>">Hapus</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal Tambah Barang -->
    <form action="/barang/simpan" method="post">
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Menambah Barang Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="barang">Nama Barang</label>
                            <input type="text" name="nama_product" class="form-control" placeholder="Masukan Nama barang baru.....">
                        </div>
                        <div class="form-group">
                            <label for="harga">Harga</label>
                            <input type="number" name="harga_product" class="form-control" placeholder="Masukan Harga Barang....">
                        </div>
                        <div class="form-group">
                            <label for="kategori">Kategori</label>
                            <select name="product_kategori" class="form-control product_kategori">
                                <option value="" disabled>-Pilih-</option>
                                <?php foreach ($kategori as $value) : ?>
                                    <option value="<?= $value->kategori_id; ?>"> <?= $value->nama_kategori; ?> </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="product_id" class="product_id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Modal Edit Product-->
    <form action="/barang/edit" method="post">
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Barang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input type="text" class="form-control nama_product" name="nama_product" ">
                        </div>

                        <div class=" form-group">
                            <label>Harga</label>
                            <input type="Number" class="form-control harga_product" name="harga_product">
                        </div>

                        <div class="form-group">
                            <label>kategori</label>
                            <select name="product_kategori" class="form-control product_kategori">
                                <option value="">-Select-</option>
                                <?php foreach ($kategori as $row) : ?>
                                    <option value="<?= $row->kategori_id; ?>"><?= $row->nama_kategori; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="product_id" class="product_id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Hapus Modal -->
    <form action="/barang/hapus" method="post">
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Hapus Barang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-labelledby="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Apakah ingin menghapus barang ini?</h4>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="product_id" class="productID">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-danger">Iya,Hapus</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.btn-edit').on('click', function() {
                const id = $(this).data('id');
                const nama = $(this).data('name');
                const harga = $(this).data('price');
                const kategori = $(this).data('category_id');

                $('.product_id').val(id);
                $('.nama_product').val(nama);
                $('.harga_product').val(harga);
                $('.product_kategori').val(kategori).trigger('change');

                $('#editModal').modal('show');
            });

            $('.btn-delete').on('click', function() {
                let id = $(this).data('id');

                $('.productID').val(id);
                $('#deleteModal').modal('show');
            })
        });
    </script>
</body>

</html>